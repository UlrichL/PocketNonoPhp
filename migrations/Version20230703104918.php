<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230703104918 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE game_state (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, fields VARCHAR(500) NOT NULL, row_count INTEGER NOT NULL, column_count INTEGER NOT NULL)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE game_state');
    }
}
