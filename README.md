# PocketNonoPhp

A simple demo project show-casing a Symfony (web) application that displays an interactive game board.

![A screenshot of a web page with the title Pocket Nono and a number of light blue or light green rectangles.](demo.png "Game board display")

It requires PHP 8 (.2) and Symfony 6.3.

The interesting files are found in `src/Controller`, `src/Service`, `templates` and `tests`.

## Setup
 
`php bin/console doctrine:migrations:migrate`

## Run With

`symfony server:start`

## Current features

Currently, this app provides the following features:

* Routing
* View rendering with Twig with dynamic (random) content
* Basic styling and interactivity on the actual page
* Accessibility for page elements (keyboard control, titles)
* Service injection (auto-wiring) and decoration
* Logging and log interception
* Unit tests

## Potential next development steps

* Use a higher-level entity (data model) with more readily available field data (non-string)
* Add more game functionality (i.e. add connection to a backend to update the game state and compare with a "desired state")
* Use (Symfony) validation with a custom Validator to assess the current game state
