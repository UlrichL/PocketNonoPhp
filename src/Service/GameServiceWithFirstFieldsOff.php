<?php

namespace App\Service;

use App\Entity\GameState;
use Symfony\Component\DependencyInjection\Attribute\AsDecorator;

/**
 * Sets all the first fields in a game as inactive.
 */
#[AsDecorator(decorates: GameService::class)]
class GameServiceWithFirstFieldsOff implements GameServiceInterface
{
    public function __construct(
        private readonly GameServiceInterface $subject,
    ) {
    }

    public function getGameState(int $lines, int $columns): GameState
    {
        $state = $this->subject->getGameState($lines, $columns);

        // NOTE this new state is not persisted; however as this is always done (first fields are always reset to inactive)...
        //   Generally the actual saving (EntityManagerInterface->flush) should probably be done only on session finish.
        for ($line = 1; $line <= $state->getRowCount(); $line++) {
            if ($state->isActive($line, 1)) {
                $state->switch($line, 1);
            }
        }

        return $state;
    }
}
