<?php

namespace App\Service;

use App\Entity\GameState;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\AsAlias;

#[AsAlias]
class GameService implements GameServiceInterface
{
    private EntityManagerInterface $em;
    private LoggerInterface $logger;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    public function getGameState(int $lines, int $columns): GameState
    {
        $repository = $this->em->getRepository(GameState::class);

        $states = $repository->findAll();

        if (count($states) > 0) {
            if ($states[0]->getRowCount() === $lines && $states[0]->getColumnCount() === $columns) {
                $this->logger->info('Using existing game state from DB');

                return $states[0];
            }
        }

        $state = new GameState();

        $state->setRowCount($lines);
        $state->setColumnCount($columns);
        $state->initializeFieldsRandomly();

        $this->em->persist($state);
        $this->em->flush();

        $this->logger->info('Created new game state');

        return $state;
    }
}
