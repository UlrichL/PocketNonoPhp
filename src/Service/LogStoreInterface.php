<?php

namespace App\Service;

interface LogStoreInterface
{
    public function getMessages(): array;
}
