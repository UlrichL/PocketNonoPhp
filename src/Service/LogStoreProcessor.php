<?php

namespace App\Service;

use Monolog\Attribute\AsMonologProcessor;
use Monolog\LogRecord;
use Symfony\Component\DependencyInjection\Attribute\AsAlias;

#[AsMonologProcessor]
#[AsAlias]
class LogStoreProcessor implements LogStoreInterface
{
    private array $messages = [];

    public function __invoke(LogRecord $record): LogRecord
    {
        if ($record->channel !== "app") {
            return $record;
        }

        $this->messages[] = $record->message;

        return $record;
    }

    public function getMessages(): array
    {
        return $this->messages;
    }
}

