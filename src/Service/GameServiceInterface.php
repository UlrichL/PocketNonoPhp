<?php

namespace App\Service;

use App\Entity\GameState;

interface GameServiceInterface
{
    public function getGameState(int $lines, int $columns): GameState;
}
