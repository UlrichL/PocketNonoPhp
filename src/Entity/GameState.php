<?php

namespace App\Entity;

use App\Repository\GameStateRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GameStateRepository::class)]
class GameState
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 500)]
    private ?string $fields = null;

    #[ORM\Column]
    private ?int $row_count = null;

    #[ORM\Column]
    private ?int $column_count = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFields(): ?string
    {
        return $this->fields;
    }

    public function setFields(string $fields): static
    {
        $this->fields = $fields;

        return $this;
    }

    public function getRowCount(): ?int
    {
        return $this->row_count;
    }

    public function setRowCount(int $row_count): static
    {
        $this->row_count = $row_count;

        return $this;
    }

    public function getColumnCount(): ?int
    {
        return $this->column_count;
    }

    public function setColumnCount(int $column_count): static
    {
        $this->column_count = $column_count;

        return $this;
    }

    public function isActive(int $row, int $column): bool
    {
        $idx = ($row-1)*$this->column_count + ($column-1);

        if ($idx < 0 || $idx >= strlen($this->fields)) {
            return false;
        }

        return $this->fields[$idx] === '1';
    }

    public function switch(int $row, int $column): void
    {
        $idx = ($row-1)*$this->column_count + ($column-1);

        if ($idx < 0 || $idx >= strlen($this->fields)) {
            return;
        }

        $this->fields[$idx] = $this->fields[$idx] === '1' ? '0' : '1';
    }

    public function initializeFieldsRandomly(): void
    {
        $this->fields = '';
        for ($i=0; $i<$this->column_count * $this->row_count - 1; $i++) {
            $this->fields .= (string)rand(0,1);
        }
    }
}
