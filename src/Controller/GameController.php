<?php

namespace App\Controller;

use App\Service\GameServiceInterface;
use App\Service\LogStoreInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends AbstractController
{
    #[Route('/', name:'main')]
    public function show(LoggerInterface $logger, GameServiceInterface $gameService, LogStoreInterface $logStore): Response
    {
        $lineCount = rand(2,4);
        $columnCount = rand(5,8);

        $logger->info("Creating a game board with $lineCount x $columnCount fields.");

        $gameState = $gameService->getGameState($lineCount, $columnCount);

        return $this->render('base.html.twig', [
            'gameState' => $gameState,
            'lineCount' => $gameState->getRowCount(),
            'columnCount' => $gameState->getColumnCount(),
            'messages' => $logStore->getMessages(),
        ]);
    }
}
