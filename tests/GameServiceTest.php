<?php

class GameServiceTest extends \PHPUnit\Framework\TestCase {
    public function testCreatesAGameStateAccordingToParameters() {
        $gameService = new \App\Service\GameService();

        $state = $gameService->getGameState(3, 6);

        $this->assertIsArray($state);
        $this->assertCount(3, $state);
        $this->assertIsArray($state[0]);
        $this->assertCount(6, $state[0]);
    }

    public function testItHandlesBogusInputParameters() {
        $gameService = new \App\Service\GameService();

        $gameService->getGameState(-1, 6);
        $gameService->getGameState(3, 0);

        $this->assertTrue(true); // Just to test that this is reached
    }
}
