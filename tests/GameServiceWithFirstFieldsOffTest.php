<?php

namespace App\Tests;
use App\Service\GameService;

class GameServiceWithFirstFieldsOffTest extends \PHPUnit\Framework\TestCase
{
    public function testItDisablesTheFirstGameFields()
    {
        $gameServiceInnerMock = $this->createMock(\App\Service\GameService::class);

        $gameServiceInnerMock
            ->expects($this->once())
            ->method('getGameState')
            ->will($this->returnValue([
                [true, false],
                [true, true],
                [false, true],
                [false, false],
            ]))
        ;

        $gameService = new \App\Service\GameServiceWithFirstFieldsOff($gameServiceInnerMock);

        $state = $gameService->getGameState(4, 2);

        $this->assertIsArray($state);
        $this->assertEquals([
            [false, false],
            [false, true],
            [false, true],
            [false, false],
        ], $state);
    }
}
